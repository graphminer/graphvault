/** 
 * Common stuff in project
 * @description: e.g. entities and operations 
 * @author: songleyi  Apr 2, 2014
 * @version: 0.1.0
 * @modify: 
 * @Copyright: 华东师范大学数据科学与工程院版权所有
 */
package edu.ecnu.graphvault.common;